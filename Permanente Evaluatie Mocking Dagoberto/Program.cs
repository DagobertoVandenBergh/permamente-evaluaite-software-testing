﻿using System;

namespace Permanente_Evaluatie_Mocking_Dagoberto
{
    class Program
    {
        static void Main(string[] args)
        {

            string ip;

            Console.WriteLine("Geef een Iddress op (xxx.xxx.xxx.xx)");
            ip = Console.ReadLine();

            var Time = new OpenWorldAPI();
            var output = new WorldTimeServices(Time);

            OpenWeatherMapApi zone = new OpenWeatherMapApi();
            var output1 = new WeatherService(zone);

            Console.WriteLine(output.GetCurrentTime(ip));

            Console.WriteLine(output1.GetCurrentWeather(output.GetCurrentRegion(ip)));


            Console.ReadLine();


        }
    }
}
