﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json;

namespace Permanente_Evaluatie_Mocking_Dagoberto
{
    public class OpenWorldAPI : IWordTimeAPI
    {
        private string url = "http://worldtimeapi.org/api/ip/";
        public DateTime GetCurrentTimeApi(string ip)
        {
            using (var httpClient = new HttpClient())
            {
                var httpRespone = httpClient.GetAsync(url + ip).GetAwaiter().GetResult();
                var response = httpRespone.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                return JsonConvert.DeserializeObject<WordTime>(response).datetime;
            }
        }

        public string GetCurrentTimeZone(string ip)
        {
            using (var httpClient = new HttpClient())
            {
                var httpRespone = httpClient.GetAsync(url + ip).GetAwaiter().GetResult();
                var response = httpRespone.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                var timezone = JsonConvert.DeserializeObject<WordTime>(response).timezone;
                return timezone;
            }
        }
    }
}
