﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Permanente_Evaluatie_Mocking_Dagoberto
{
     public class WorldTimeServices
    {
        private readonly IWordTimeAPI iwordTimeAPI;

        public WorldTimeServices(IWordTimeAPI iwordTimeAPI)
        {
            this.iwordTimeAPI = iwordTimeAPI;
        }

        public string GetCurrentTime(string ip)
        {
            var time = iwordTimeAPI.GetCurrentTimeApi(ip);

            if (time.Hour > 0 && time.Hour < 7)
            {
                return "Nacht";
            }
            else if (time.Hour >= 7 && time.Hour < 12)
            {
                return "Voormiddag";
            }
            else if (time.Hour >= 12 && time.Hour < 13)
            {
                return "Middag";
            }
            else if (time.Hour >= 13 && time.Hour < 17)
            {
                return "Namiddag";
            }
            else
            {
                return "Avond";
            }
        }

        public string GetCurrentRegion(string ip)
        {
            var time = iwordTimeAPI.GetCurrentTimeZone(ip);
            var region = time.Split("/");
            return region[1];
        }
    }
}
