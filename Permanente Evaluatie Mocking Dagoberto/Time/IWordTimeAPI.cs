﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json;

namespace Permanente_Evaluatie_Mocking_Dagoberto
{
     public interface IWordTimeAPI
    {
        DateTime GetCurrentTimeApi(string ip);

        string GetCurrentTimeZone(string ip);
    }
}
