﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Permanente_Evaluatie_Mocking_Dagoberto
{
    public class OpenWeatherMapApi : IOpenWeatherMapApi
    {
        private string url1 = "http://api.openweathermap.org/data/2.5/weather?q=";
        private string url2 = "&appid=b1a90ec4d94d84ecf2a3f2bb634b970d&units=metric";

        public float GetCurrentTemperatureInAntwerp(string region)
        {
            using (var httpClient = new HttpClient())
            {
                var httpRespone = httpClient.GetAsync(url1+ region + url2).GetAwaiter().GetResult();
                var response = httpRespone.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                return JsonConvert.DeserializeObject<OpenWeather>(response).main.temp;
            }
        }
    }
}
