﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Permanente_Evaluatie_Mocking_Dagoberto
{
    public interface IOpenWeatherMapApi
    {
        float GetCurrentTemperatureInAntwerp(string region);
    }
}
