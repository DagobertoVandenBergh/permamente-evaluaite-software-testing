﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Permanente_Evaluatie_Mocking_Dagoberto;

namespace Permanente_Evaluatie_Mocks_Dagoberto.Tests
{
    [TestClass]
     public class WorldTimeServicesTests
    {
        [TestMethod]
        public void CurrentTimeNight()
        {
            string ip = "195.130.131.38";
            string test = "2020-11-13T06:54:48.224225+01:00";

            var openworldtime = new Mock<IWordTimeAPI>();
            openworldtime.Setup(x => x.GetCurrentTimeApi(ip)).Returns(Convert.ToDateTime(test));

            //Act
            var WorldTimeServices = new WorldTimeServices(openworldtime.Object);

            //Assert
            Assert.AreEqual("Nacht", WorldTimeServices.GetCurrentTime(ip));
        }


        [TestMethod]
        public void CurrentTimeMornig()
        {
            string ip = "195.130.131.38";
            string test = "2020-11-13T11:54:48.224225+01:00";
            var openworldtime = new Mock<IWordTimeAPI>();
            openworldtime.Setup(x => x.GetCurrentTimeApi(ip)).Returns(Convert.ToDateTime(test));

            //Act
            var WorldTimeServices = new WorldTimeServices(openworldtime.Object);

            //Assert
            Assert.AreEqual("Voormiddag", WorldTimeServices.GetCurrentTime(ip));
        }

        [TestMethod]
        public void CurrentTimeMidDay()
        {
            string ip = "195.130.131.38";
            string test = "2020-11-13T12:54:48.224225+01:00";
            var openworldtime = new Mock<IWordTimeAPI>();
            openworldtime.Setup(x => x.GetCurrentTimeApi(ip)).Returns(Convert.ToDateTime(test));

            //Act
            var WorldTimeServices = new WorldTimeServices(openworldtime.Object);

            //Assert
            Assert.AreEqual("Middag", WorldTimeServices.GetCurrentTime(ip));
        }

        [TestMethod]
        public void CurrentTimeAfterNoon()
        {
            string ip = "195.130.131.38";
            string test = "2020-11-13T15:54:48.224225+01:00";
            var openworldtime = new Mock<IWordTimeAPI>();
            openworldtime.Setup(x => x.GetCurrentTimeApi(ip)).Returns(Convert.ToDateTime(test));

            //Act
            var WorldTimeServices = new WorldTimeServices(openworldtime.Object);

            //Assert
            Assert.AreEqual("Namiddag", WorldTimeServices.GetCurrentTime(ip));
        }

        [TestMethod]
        public void CurrentTimeMidnight()
        {
            string ip = "195.130.131.38";
            string test = "2020-11-13T18:54:48.224225+01:00";
            var openworldtime = new Mock<IWordTimeAPI>();
            openworldtime.Setup(x => x.GetCurrentTimeApi(ip)).Returns(Convert.ToDateTime(test));

            //Act
            var WorldTimeServices = new WorldTimeServices(openworldtime.Object);

            //Assert
            Assert.AreEqual("Avond", WorldTimeServices.GetCurrentTime(ip));
        }

        public void Zone() 
        {
            string ip = "195.130.131.38";
            
            var openworldtime = new Mock<IWordTimeAPI>();
            openworldtime.Setup(x => x.GetCurrentTimeZone(ip)).Returns("Eupope/Brussels");

            //Act
            var WorldTimeServices = new WorldTimeServices(openworldtime.Object);

            //Assert
            Assert.AreEqual("Brussels", WorldTimeServices.GetCurrentRegion(ip));
        }

    }
}
